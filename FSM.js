/**
 * Created by Voin on 04.08.16.
 */
'use strict';

/**
 * @dependencies
 *  async is used to organize live queue
 *  chalk is used just for beauty :P
 */
const async = require('async');
const chalk = require('chalk');

/**
 * @class AsyncFSM
 * @todo Allow to pass state-table and initial state as parameter to constructor
 * @todo Add *Back state that will be rolling FSM to __previousState
 * @example
 *
 let human = new AsyncFSM();
 human.emit('fsm-accident');

 // You can pass custom parameters to each action
 human.emit('fsm-funeral', {
    actionParams: {
       text: 'This is last words, srsly'
    }
 });
 human.emit('fsm-alive');

 human.emit('fsm-sleepy');
 human.emit('fsm-dream');

 // You also can pass custom callback function as third param
 // that will be invoked when transition finished
 human.emit('fsm-wake-up', {}, (err) => {
     if (err) {
        console.log(err);
        return;
     }
     console.log('Finally! Morning!');
 });

 human.emit('fsm-hunger');
 human.emit('fsm-eat');
 human.emit('fsm-sick');
 human.emit('fsm-funeral');
 *
 */
class AsyncFSM {
    constructor() {
        this.__currentState = '';
        this.__previousState = '';
        this.__initialState = 'Stand';
        this.__currentState = this.__initialState;

        /**
         * @description Cone of FSM - async.queue is queue system that organize async flow
         * @see {@url http://caolan.github.io/async/docs.html#.queue}
         * @private
         */
        this.__transitionQueue = async.queue(this.doTransition.bind(this), 1);
        this.__states = {
            'Stand': {
                transitions: {
                    'fsm-go'       : { newState: 'Walk' },
                    'fsm-accident' : { newState: 'Die' },
                    'fsm-maniac'   : { newState: 'walk' },
                    'fsm-hunger'   : { newState: 'Lunch' },
                    'fsm-sleepy'   : { newState: 'Sleep' },
                    'fsm-think'    : { action: this.generateIdea.bind(this), newState: 'Stand' }
                }
            },
            'Walk': {
                on_enter: this.accelerate.bind(this),
                on_exit: this.slowDown.bind(this),
                transitions: {
                    'fsm-accident' : { action: this.die.bind(this), newState: 'Die' },
                    'fsm-tire'     : { newState: 'Stand' },
                    'fsm-hunger'   : { action: this.eat.bind(this), newState: 'Lunch' }
                }
            },
            'Lunch': {
                on_enter: this.cook.bind(this),
                on_exit: this.cleanup.bind(this),
                transitions: {
                    'fsm-eat'      : { action: this.eat.bind(this), newState: 'Lunch' },
                    'fsm-accident' : { action: this.die.bind(this), newState: 'Die' },
                    'fsm-sick'     : { action: this.die.bind(this), newState: 'Die' },
                    'fsm-full'     : { newState: 'Stand' }
                }
            },
            'Sleep': {
                on_enter: this.haveShower.bind(this),
                on_exit: this.makeExercise.bind(this),
                transitions: {
                    'fsm-dream'     : { action: this.dream.bind(this), newState: 'Sleep' },
                    'fsm-nightmare' : { newState: 'Stand' },
                    'fsm-wake-up'   : { newState: 'Stand' }
                }
            },
            'Die': {
                on_enter: this.die.bind(this),
                on_exit: this.alive.bind(this),
                transitions: {
                    'fsm-funeral': { action: this.cry.bind(this), newState: 'Die' },
                    'fsm-alive': { newState: 'Stand' }
                }
            }
        };
    }

    /**
     * @returns {String} - Current state of FSM
     */
    get currentState() {
        return this.__currentState;
    }

    /**
     * @description Register transition in queue
     * @param eventName           {String}    - FSM event to schedule
     * @param params              {Object=}   - params that will be passed to transition
     * @param params.actionParams {Object=}   - params that will be passed to action function
     * @param params.exitParams   {Object=}   - params that will be passed to onExit function
     * @param params.enterParams  {Object=}   - params that will be passed to onEnter function
     * @param callback            {Function=} - Callback that will be invoked when transition end, if no callback passed transitionFinished func will be invoked
     * @public
     */
    emit(eventName, params, callback) {
        if (callback && typeof callback === 'function') {
            this.__transitionQueue.push({
                name: eventName,
                params: params || {}
            }, callback.bind(this));
        } else {
            this.__transitionQueue.push({
                name: eventName,
                params: params || {}
            }, this.transitionFinished.bind(this));
        }
    }

    /**
     * @description Performs FSM transition
     * @param event        {Object}   - jop to push to queue
     * @param event.name   {String}   - FSM event
     * @param event.params {String=}  - FSM event
     * @param callback     {Function} - callback that will be invoked when transition finished
     * @private
     */
    doTransition(event, callback) {

        /**
         * @description Try to build transition for passed event
         */
        try {
            var transition = this.buildTransition(event);
        } catch(err) {
            return callback(err);
        }

        /**
         * @description Perform FSM transition
         */
        async.series([
            /**
             * @description Execute transition action
             * @param next {Function} - callback to exec when step complete
             */
            (next) => {
                if (!transition.action) { return next(); }
                transition.action(transition.actionParams, next.bind(this));
            },
            /**
             * @description Execute action on exit
             * @param next {Function} - callback to exec when step complete
             */
            (next) => {
                if (!transition.onExit) { return next(); }
                transition.onExit(transition.exitParams, next.bind(this));
            },
            /**
             * @description Set new state
             * @param next {Function} - callback to exec when step complete
             */
            (next) => {
                this.__previousState = this.currentState;
                this.__currentState = transition.newState;
                return next();
            },
            /**
             * @description Execute action on enter
             * @param next {Function} - callback to exec when step complete
             */
            (next) => {
                if (!transition.onEnter) { return next(); }
                transition.onEnter(transition.enterParams, next.bind(this));
            }
        ], callback.bind(this));
    }

    /**
     * @description Builds transition object that contains information for FSM-transition
     * @param event {Object} - event object to build transition from
     * @returns {Object} - FSM transition object
     */
    buildTransition(event) {
        if (!event || !event.name) {
            throw new TypeError('No event specified');
        }

        let transition = {};

        if (!this.__states[this.currentState].transitions) {
            throw new TypeError('No transition field specified for state, please check states table');
        }

        /**
         * @description Configuration for FSM event based on events hash-table
         * @type {Object}
         */
        const eventConfig = this.__states[this.currentState].transitions[event.name];

        if (!eventConfig) {
            throw new TypeError('No such event in current state');
        }

        /**
         * @description Set target state for transition, if no one specified in config set current state
         * @type {String}
         */
        transition.newState = eventConfig.newState ? eventConfig.newState : this.currentState;

        /**
         * @description If there is change of state add onExit and onEnter functions to transition
         */
        transition.onExit  = (this.currentState !== transition.newState) ? this.__states[this.currentState].on_exit    : null;
        transition.onEnter = (this.currentState !== transition.newState) ? this.__states[transition.newState].on_enter : null;

        /**
         * Add event action from event config
         */
        transition.action = eventConfig.action || null;

        /**
         * @description Add transition event
         */
        transition.event = event;

        /**
         * @description Add action params
         */
        transition.actionParams = event.params.actionParams || {};
        transition.exitParams   = event.params.exitParams   || {};
        transition.enterParams  = event.params.enterParams  || {};

        return transition;

    }

    /**
     * @description This function will be executed every time some transition complete or failed
     * @param err {Error} - Error while performing transition
     */
    transitionFinished(err) {
        let divider = chalk.blue('---------------------------------------------------');

        if (err) {
            let errorNotification = chalk.gray('FSM transition failed, error:');
            let errorExplanation = chalk.red(err);
            console.log(divider);
            console.log(errorNotification, errorExplanation);
            console.log(divider);
            return;
        }

        let stateChangedNotigication = chalk.blue('FSM transition finished, current state is:');
        let currentState = chalk.green(this.__currentState);

        console.log(divider);
        console.log(stateChangedNotigication, currentState);
        console.log(divider);
    }

    accelerate(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('Accelerating...');
        setTimeout(callback.bind(this), 1000);
    }
    slowDown(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('Stopping...');
        setTimeout(callback.bind(this), 4000);
    }
    generateIdea(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('London is a capital of Great Britain!');
        setTimeout(callback.bind(this), 2000);
    }
    cook(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log("Bacon pancakes, makin' bacon pancakes");
        console.log("Take some bacon and I'll put it in a pancake,");
        console.log("Bacon pancakes, that's what it's gonna make,");
        console.log("Bacon pancaaake!");
        setTimeout(callback.bind(this), 500);
    }
    cleanup(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('It rubs the lotion on its skin. It does this whenever it is told.');
        setTimeout(callback.bind(this), 1000);
    }
    walk(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('Here comes Dat Boi! Oh Shit! Waddup!!!');
        setTimeout(callback.bind(this), 900);
    }
    eat(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log("I do wish we could chat longer, but... I'm having an old friend for dinner. Bye.");
        setTimeout(callback.bind(this), 2000);
    }
    dream(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('I got the nasty in my taxi...');
        setTimeout(() => {
            console.log('you need a lift...');
            setTimeout(() => {
                console.log('You can sit between the backseat');
                setTimeout(() => {
                    console.log('...and my dick');
                    setTimeout(callback.bind(this), 200);
                }, 600);
            }, 600);
        }, 600);
    }
    haveShower(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log("I'm smoking cigarettes in the shower");
        console.log("When they get wet I just light another");
        setTimeout(callback.bind(this), 800);
    }
    makeExercise(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log('Will be tomorrow...eh');
        setTimeout(callback.bind(this), 2000);
    }
    die(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log("Question: If I died in my apartment like a rat in a cage");
        console.log("Would the neighbors smell the corpse before the cat ate my face?");
        setTimeout(callback.bind(this), 1000);
    }
    alive(params, callback) {
        console.log(chalk.cyan('Says:'));
        console.log("Whether you're a brother or whether you're a mother");
        console.log("You're stayin' alive, stayin' alive");
        console.log("Feel the city breakin' and everybody shakin'");
        console.log("And we're stayin' alive, stayin' alive");
        console.log("Ah, ha, ha, ha, stayin' alive, stayin' alive");
        console.log("Ah, ha, ha, ha, stayin' alive");
        setTimeout(callback.bind(this), 1000);
    }
    cry(params, callback) {
        console.log(chalk.cyan('Says:'));
        if (params.text) {
            console.log(params.text);
        } else {
            console.log("Take me out tonight");
            console.log("Where there's music and there's people");
            console.log("And they're young and alive");
        }
        setTimeout(callback.bind(this), 200);
    }
}

module.exports = AsyncFSM;
